#ifndef ADC_H_
#define ADC_H_

#include <stdio.h>

#include "xadcps.h"
#include "xil_types.h"

class ADC {
public:
	ADC() {
		int xStatus;

		/*
		 * Looks up the device configuration based on the unique device ID. The
		 * table contains the configuration info for each device in the system.
		 */
		XAdcPsConfigPtr = XAdcPs_LookupConfig(XPAR_XADC_WIZ_0_DEVICE_ID);
		if(XAdcPsConfigPtr == NULL) {
			printf("XAdcPs_LookupConfig() failed\n\r");
		}

		/* This function initializes a specific XAdcPs device/instance. */
		xStatus = XAdcPs_CfgInitialize(&XAdcPsInst, XAdcPsConfigPtr,XAdcPsConfigPtr->BaseAddress);
		if(xStatus != XST_SUCCESS) {
			printf("XAdcPs_CfgInitialize() failed\r\n");
		}
	}

	/*
	 * This function return a sample read by the ADC. The sample is 16
	 * bit long.
	 */
	u16 Sample() {
		return (XAdcPs_GetAdcData(&XAdcPsInst, XADCPS_CH_VPVN));
	}

private:
	XAdcPs_Config * XAdcPsConfigPtr;
	XAdcPs XAdcPsInst;
};

#endif /* ADC_H_ */
