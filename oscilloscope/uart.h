#ifndef UART_H_
#define UART_H_

#include <stdio.h>

#include "xuartps.h"

class UART {
public:
	UART(int BaudRate = 115200) {
		int xStatus;

		/*
		 * Looks up the device configuration based on the unique device ID. The
		 * table contains the configuration info for each device in the system.
		 */
		XUartPsConfigPtr = XUartPs_LookupConfig(XPAR_XUARTPS_0_DEVICE_ID);
		if(XUartPsConfigPtr == NULL) {
			printf("XUartPs_LookupConfig() failed\r\n");
		}

		/*
		 * Initializes a specific XUartPs instance such that it is ready to be
		 * used. The data format of the device is setup for 8 data bits, 1
		 * stop bit, and no parity by default. The baud rate is set to a
		 * default value specified by Config->DefaultBaudRate if set, otherwise
		 * it is set to 19.2K baud.
		 */
		xStatus = XUartPs_CfgInitialize(&XUartPsInst, XUartPsConfigPtr,
				XUartPsConfigPtr->BaseAddress);
		if(xStatus != XST_SUCCESS) {
			printf("XUartPs_CfgInitialize() failed\r\n");
		}

		/* Sets the baud rate. */
		XUartPs_SetBaudRate(&XUartPsInst, BaudRate);
	}

	~UART() {}

	/*
	 * This function transmits the NumBytes first bytes of the string BufferPtr
	 * through the UART.
	 */
	unsigned int Send(u8 * BufferPtr, unsigned int NumBytes) {
		return (XUartPs_Send(&XUartPsInst, BufferPtr, NumBytes));
	}

private:
	XUartPs_Config * XUartPsConfigPtr;
	XUartPs XUartPsInst;
};

#endif /* UART_H_ */
