#include "platform.h"
#include "xgpio.h"
#include "xparameters.h"
#include "sleep.h"

#define LED_CHANNEL 1

int main()
{
	XGpio Gpio;
	u32 tmp;

	/* Initializes platform peripherals */
	XGpio_Initialize(&Gpio, XPAR_GPIO_0_DEVICE_ID);
	init_platform();

	/* Writes string to the UART */
	print("Hello World\n\r");

	tmp = 0x00000000;

	while(1) {
		/* Blinks LEDs */
		XGpio_DiscreteWrite(&Gpio, LED_CHANNEL, tmp);
		tmp = ~tmp;
		/* Sleeps for 1000000 us */
		usleep(1000000);
	}

	return 0;
}